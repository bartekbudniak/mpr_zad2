package mpr_zad2.mpr_zad2;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class OrderItemTest {
	
	ItemManager itemManager = new ItemManager();
	
	private final static String NAME_1 = "ksiazka";
	private final static String DESCRIPTION_1 = "ksiazka o gotowaniu";
	private final static double PRICE_1 = 45.65;
	
	@Test
	public void checkConnection(){
		assertNotNull(itemManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		OrderItem item = new OrderItem(NAME_1, DESCRIPTION_1, PRICE_1);
		
		itemManager.clearItems();
		assertEquals(1,itemManager.addItem(item));
		
		List<OrderItem> items = itemManager.getAllItems();
		OrderItem itemsRetrieved = items.get(0);
		
		assertEquals(NAME_1,itemsRetrieved.getName());
		assertEquals(DESCRIPTION_1,itemsRetrieved.getDescription());		
		assertEquals(String.valueOf(PRICE_1),itemsRetrieved.getPrice()); 
		
	}

}

