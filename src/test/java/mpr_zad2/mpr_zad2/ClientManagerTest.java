package mpr_zad2.mpr_zad2;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class ClientManagerTest {
	
	ClientManager clientManager = new ClientManager();
	
	private final static String NAME_1 = "Jan";
	private final static String SURNAME_1 = "Nowak";	
	private final static String LOGIN_1 = "Janek123";
	
	@Test
	public void checkConnection(){
		assertNotNull(clientManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		ClientDetails client = new ClientDetails(NAME_1, SURNAME_1, LOGIN_1);
		
		clientManager.clearClients();
		assertEquals(1,clientManager.addClient(client));
		
		List<ClientDetails> clients = clientManager.getAllClients();
		ClientDetails clientRetrieved = clients.get(0);
		
		assertEquals(NAME_1, clientRetrieved.getName());
		assertEquals(SURNAME_1, clientRetrieved.getSurname());
		assertEquals(LOGIN_1, clientRetrieved.getLogin());
		
	}

}