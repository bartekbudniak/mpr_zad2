package mpr_zad2.mpr_zad2;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class AddressManagerTest {
	
	AddressManager addressManager = new AddressManager();
	
	private final static String STREET_1 = "sosnowa";
	private final static String BUILDINGNUMBER_1 = "15";
	private final static String FLATNUMBER_1 = "1a";
	private final static String POSTALCODE_1 = "80-850";
	private final static String CITY_1 = "Warszawa";
	private final static String COUNTRY_1 = "Polska";
	
	@Test
	public void checkConnection(){
		assertNotNull(addressManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		Address address = new Address(STREET_1, BUILDINGNUMBER_1, FLATNUMBER_1, POSTALCODE_1, CITY_1, COUNTRY_1);
		
		addressManager.clearAddresses();
		assertEquals(1,addressManager.addAddress(address));
		
		List<Address> addresses = addressManager.getAllAddresses();
		Address addressRetrieved = addresses.get(0);
		
		assertEquals(STREET_1, addressRetrieved.getStreet());
		assertEquals(BUILDINGNUMBER_1, addressRetrieved.getBuildingNumber());
		assertEquals(FLATNUMBER_1, addressRetrieved.getFlatNumber());
		assertEquals(POSTALCODE_1, addressRetrieved.getPostalCode());
		assertEquals(CITY_1, addressRetrieved.getCity());
		assertEquals(COUNTRY_1, addressRetrieved.getCountry());
		
	}

}
