package mpr_zad2.mpr_zad2;

public class ClientDetails {
	private long id;
	private String name;
	private String surname;
	private String login;
	
	public ClientDetails(){
		
	}
	public ClientDetails(String name, String surname, String login){
		super();
		this.name = name;
		this.surname = surname;
		this.login = login;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	
	
}
